// = require plugin/flot/jquery.flot.cust
// = require plugin/flot/jquery.flot.resize
// = require plugin/flot/jquery.flot.time
// = require plugin/flot/jquery.flot.tooltip.min
// = require plugin/flot/jquery.flot.fillbetween
// = require plugin/flot/jquery.flot.orderBar.min
// = require plugin/flot/jquery.flot.pie
//= require sparkline/jquery.sparkline.min.js
//= require chartjs/Chart.min.js
//= require morris/raphael-2.1.0.min.js
//= require morris/morris.js
//= require rickshaw/vendor/d3.v3.js
//= require rickshaw/rickshaw.min.js
//= require chartist/chartist.min.js

//--- Traducciones para el mensaje de toast
let message_toast = document.getElementById('message_toast').value,
    message_toast2 = document.getElementById('message_toast2').value;

setTimeout(function(){

    $('#users_sel_chosen, #states_sel_chosen').css('width','110%');
    var hijo = $('.search-field').children();

    hijo.css('width', '110%')

}, 2000);

$('#search_us, #search').on('click', function(){

    $('#states, #users').modal('hide');

    let msnTraduccion = document.getElementById('mensaje_load').value,
        titleTradu = document.getElementById('titleSwal').value;
    var mensaje = `<p class='saving'>${msnTraduccion}<span>.</span><span>.</span><span>.</span></p>`;
    swal({
        html: true,
        title: `${titleTradu}`,
        text: "" + mensaje + "",
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showCancelButton: false,
        showConfirmButton: false
    }) ;
});

//Resultados de la busqueda
function resultados( result ){

    if ( result > 0 ){
        icon = 'success';
        txt_result = `Total de resultados ${result}`;
        $('.content_description').css('display','block');
    } else {
        icon = 'error';
        txt_result = `No existen resultados`;
        $('.content_description').css('display','none');
    }

    swal({
        title: `Resultados`,
        text: `${txt_result}`,
        type: `${icon}`
    });
}

//-- Exportar a excel
$(document).on('click', ".xls_g", function () {
    let idioma = $("#idioma").val(),
        busqueda = $("#busqueda").val(),
        filtro_generador = $("#filtro_generador").val(),
        user_id = $("#currentUserId").val();

    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        // url: "/niv_alertamiento/index?xls_g=true" + "&user_id="+ user_id + "&idioma=" + idioma + params,
        url: '../niv_alertamiento/exportar_niveles',
        data: {
            query: busqueda,
            languaje: idioma,
            user_export_id: user_id,
            filtro_gen: filtro_generador
        },
    });

    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: 'slideDown',
        timeOut: 5000
    };
    toastr.success(`${message_toast}`, `${message_toast2}`);
})
