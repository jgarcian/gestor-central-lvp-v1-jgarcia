class FiltersnosController < ApplicationController
  before_action :set_filtersno, only: [:show, :edit, :update, :destroy]

  # GET /filtersnos
  # GET /filtersnos.json
  def index

    if current_user
      @cu = current_user.profile_id

      if @cu == 1
        @tx_sessions = TxSessions.all.page(params[:page] || 1).per(10)
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end

  end


  def consultaFiltros

    @user_id = current_user.id
    @hoy = Time.now
    @fecha = @hoy.strftime("%Y-%m-%d")
    @hora = @hoy.strftime("%H:%M:%S:%L")

    @arrFiltros = Array.new

    if params[:agregar_todos]
      @tx_filters = TxFilters.all
      # render :json => @tx_filters


      @tx_filters.each do | fil |
        @existeFiltro = Filtersno.where(:IdFiltro => fil.Id)

        # Si no existe el filtro se agrega en la base
        if !@existeFiltro.present?
          @listaNegraFiltro =  Filtersno.new
          @listaNegraFiltro.IdFiltro = fil.Id
          @listaNegraFiltro.IdUsuario = @user_id
          @listaNegraFiltro.Fecha = @fecha
          @listaNegraFiltro.save

          @arrFiltros.push(@listaNegraFiltro)
        end
      end

      @mensaje = Tbitacora.new
      @mensaje.IdUsuario = @user_id
      @mensaje.Fecha = @fecha
      @mensaje.Hora = @hora
      @mensaje.Mensaje = "Se agregaron multiples filtros: #{@arrFiltros.length} en total"
      @mensaje.save

      render :json => @arrFiltros
    elsif params[:eliminar_todos]

      @filtros_bl = Filtersno.all
      @filtros_bl.each do |fil|
        fil.destroy
      end

      @mensaje = Tbitacora.new
      @mensaje.IdUsuario = @user_id
      @mensaje.Fecha = @fecha
      @mensaje.Hora = @hora
      @mensaje.Mensaje = "Se eliminaron multiples filtros: #{@filtros_bl.length} en total"
      @mensaje.save

      render :json => @filtros_bl
    end

  end

  # GET /filtersnos/1
  # GET /filtersnos/1.json
  def show
  end

  # GET /filtersnos/new
  def new
    @filtersno = Filtersno.new
  end

  # GET /filtersnos/1/edit
  def edit
  end

  # POST /filtersnos
  # POST /filtersnos.json
  def create

    @idFiltro = params[:filtersno][:IdFiltro]

    @existeFiltro = Filtersno.where(:IdFiltro => @idFiltro)

    if !@existeFiltro.present?

      @user_id = current_user.id
      @hoy = Time.now
      @fecha = @hoy.strftime("%Y-%m-%d")
      @hora = @hoy.strftime("%H:%M:%S:%L")

      @filtersno = Filtersno.new(filtersno_params)
      @filtersno.IdUsuario = @user_id
      @filtersno.Fecha = @fecha

      if @filtersno.save
        @mensaje = Tbitacora.new
        @mensaje.IdUsuario = @user_id
        @mensaje.Fecha = @fecha
        @mensaje.Hora = @hora
        @mensaje.Mensaje = "Se agrego a la lista el filtro #{@idFiltro}"
        @mensaje.save
      end

      render :json => @filtersno
    end

    # respond_to do |format|
    #   if @filtersno.save
    #     format.html { redirect_to @filtersno, notice: 'Filtersno was successfully created.' }
    #     format.json { render :show, status: :created, location: @filtersno }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @filtersno.errors, status: :unprocessable_entity }
    #   end
    # end

  end

  # PATCH/PUT /filtersnos/1
  # PATCH/PUT /filtersnos/1.json
  def update
    respond_to do |format|
      if @filtersno.update(filtersno_params)
        format.html { redirect_to @filtersno, notice: 'Filtersno was successfully updated.' }
        format.json { render :show, status: :ok, location: @filtersno }
      else
        format.html { render :edit }
        format.json { render json: @filtersno.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /filtersnos/1
  # DELETE /filtersnos/1.json
  def destroy
    @filtersno.destroy


    @user_id = current_user.id
    @hoy = Time.now
    @fecha = @hoy.strftime("%Y-%m-%d")
    @hora = @hoy.strftime("%H:%M:%S:%L")

    @mensaje = Tbitacora.new
    @mensaje.IdUsuario = @user_id
    @mensaje.Fecha = @fecha
    @mensaje.Hora = @hora
    @mensaje.Mensaje = "Se elimino de la lista el filtro #{@filtersno.IdFiltro}"
    @mensaje.save

    # respond_to do |format|
    #   format.html { redirect_to filtersnos_url, notice: 'Filtersno was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_filtersno
      @filtersno = Filtersno.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def filtersno_params
      params.require(:filtersno).permit(:IdFiltro, :IdUsuario, :Fecha)
    end
end
