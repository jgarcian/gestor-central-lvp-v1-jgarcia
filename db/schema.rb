# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190226220949) do

  create_table "areas", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "area_id"
  end

  create_table "bitacora_white_lists", force: :cascade do |t|
    t.integer  "IdUsuario"
    t.boolean  "Estado"
    t.integer  "IdLista"
    t.string   "Hora"
    t.string   "Fecha"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "filtersnos", force: :cascade do |t|
    t.integer  "IdFiltro"
    t.integer  "IdUsuario"
    t.string   "Fecha"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "old_passwords", force: :cascade do |t|
    t.string   "encrypted_password",       null: false
    t.string   "password_archivable_type", null: false
    t.integer  "password_archivable_id",   null: false
    t.datetime "created_at"
    t.string   "password_salt"
    t.index ["password_archivable_type", "password_archivable_id"], name: "index_password_archivable"
  end

  create_table "permissions", force: :cascade do |t|
    t.integer  "view_id"
    t.string   "view_name"
    t.integer  "crear"
    t.integer  "editar"
    t.integer  "leer"
    t.integer  "eliminar"
    t.integer  "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "name"
    t.integer  "flag"
    t.integer  "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "referenciadealerta", force: :cascade do |t|
    t.integer  "idAlerta"
    t.integer  "idCampo"
    t.string   "campo"
    t.string   "valor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "talertaxusr", primary_key: ["IdGrupo", "IdPerfil", "IdAlerta", "Prioridad", "IdUsuario"], force: :cascade do |t|
    t.integer "IdGrupo",   limit: 2,  null: false
    t.integer "IdPerfil",  limit: 2,  null: false
    t.integer "IdAlerta",  limit: 2,  null: false
    t.integer "Prioridad", limit: 2,  null: false
    t.varchar "IdUsuario", limit: 12, null: false
  end

  create_table "taplicacioneskm", force: :cascade do |t|
    t.integer  "IdAplicacion"
    t.varchar  "Nombre",       limit: 1000
    t.varchar  "Descripcion",  limit: 1000
    t.varchar  "Version",      limit: 1000
    t.integer  "idProducto"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tatendidasxhoy", force: :cascade do |t|
    t.varchar  "fecha",      limit: 1000
    t.integer  "idProducto"
    t.integer  "cantidad"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "tatendiendos", force: :cascade do |t|
    t.varchar  "IdUsuario",  limit: 255
    t.varchar  "IdTran",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "tbitacora", primary_key: "Id", force: :cascade do |t|
    t.integer    "IdProducto",    limit: 2
    t.varchar    "IdUsuario",     limit: 12
    t.integer    "IdLay",         limit: 2
    t.integer    "IdFormato",     limit: 2
    t.integer    "IdGrupo",       limit: 2
    t.integer    "IdPerfil",      limit: 2
    t.integer    "IdAlerta",      limit: 2
    t.integer    "IdFiltro",      limit: 2
    t.integer    "IdAplicacion",  limit: 2
    t.integer    "IdTipoReg",     limit: 2
    t.varchar    "IP",            limit: 39
    t.varchar    "NombrePC",      limit: 15
    t.varchar    "Fecha",         limit: 10
    t.varchar    "Hora",          limit: 14
    t.varchar    "IdTran",        limit: 80
    t.text_basic "Comparacion",   limit: 2147483647
    t.integer    "Escalamiento",  limit: 2
    t.text_basic "Mensaje",       limit: 2147483647
    t.integer    "Limite"
    t.integer    "Tiempo"
    t.text_basic "Referencia",    limit: 2147483647
    t.text_basic "Condicion",     limit: 2147483647
    t.integer    "IdEstado",      limit: 2
    t.integer    "IdPrioridad",   limit: 2
    t.integer    "FalsoPositivo", limit: 2
    t.integer    "FactorRiesgo"
    t.integer    "IdProblema"
    t.integer    "canceladas",    limit: 2
    t.boolean    "habilitado"
  end

  create_table "tbitaobse", primary_key: "Id", force: :cascade do |t|
    t.integer "IdAlerta",                       null: false
    t.varchar "Fecha",             limit: 10,   null: false
    t.varchar "Hora",              limit: 14,   null: false
    t.varchar "Observaciones",     limit: 1000, null: false
    t.integer "IdEstado",          limit: 2,    null: false
    t.varchar "IdUsuarioAtencion", limit: 12,   null: false
    t.integer "falso_positivo"
  end

  create_table "tcamposformato", primary_key: ["IdLay", "IdFormato", "IdCampo"], force: :cascade do |t|
    t.integer "IdLay",         limit: 2, null: false
    t.integer "IdFormato",     limit: 2, null: false
    t.integer "IdConsecutivo", limit: 2, null: false
    t.integer "IdCampo",       limit: 2, null: false
    t.integer "TipoCampo",     limit: 2, null: false
  end

  create_table "tcamposlay", primary_key: ["IdLay", "IdCampo"], force: :cascade do |t|
    t.integer "IdLay",         limit: 2,  null: false
    t.integer "IdCampo",       limit: 2,  null: false
    t.varchar "Nombre",        limit: 45, null: false
    t.integer "Posicion",      limit: 2,  null: false
    t.integer "Longitud",      limit: 2,  null: false
    t.integer "TipoDato",      limit: 2,  null: false
    t.integer "Nivel",         limit: 2,  null: false
    t.integer "Padre",                    null: false
    t.integer "SeSustituye",              null: false
    t.varchar "Alias",         limit: 45, null: false
    t.integer "CampoLongitud", limit: 2,  null: false
  end

  create_table "tcampostoken", primary_key: ["IdLay", "IdFormato", "IdToken", "IdCampo"], force: :cascade do |t|
    t.integer "IdLay",     limit: 2, null: false
    t.integer "IdFormato", limit: 2, null: false
    t.varchar "IdToken",   limit: 5, null: false
    t.integer "IdCampo",   limit: 2, null: false
  end

  create_table "tcamposxgrupo", primary_key: ["IdGrupo", "IdConsecutivo"], force: :cascade do |t|
    t.integer "IdGrupo",       limit: 2, null: false
    t.integer "IdConsecutivo", limit: 2, null: false
    t.integer "IdCampo",       limit: 2, null: false
    t.varchar "IdToken",       limit: 5
  end

  create_table "tcategoria", primary_key: "IdCategoria", force: :cascade do |t|
    t.varchar "Descripcion", limit: 60, null: false
    t.integer "Valor",                  null: false
  end

  create_table "tdeftoken", primary_key: ["IdLay", "IdToken", "IdCampo"], force: :cascade do |t|
    t.integer "IdLay",          limit: 2,  null: false
    t.varchar "IdToken",        limit: 5,  null: false
    t.integer "IdCampo",        limit: 2,  null: false
    t.varchar "Nombre",         limit: 50
    t.integer "Longitud",       limit: 2
    t.integer "TipoDato",       limit: 2
    t.integer "LongDespliegue", limit: 2
    t.varchar "Alias",          limit: 50
    t.integer "Posicion"
  end

  create_table "tdtokens", primary_key: ["Id_Lay", "Id_Token", "Id_Campo"], force: :cascade do |t|
    t.integer "Id_Lay",          limit: 2,  null: false
    t.varchar "Id_Token",        limit: 5,  null: false
    t.integer "Id_Campo",        limit: 2,  null: false
    t.varchar "Nombre",          limit: 50
    t.integer "Longitud",        limit: 2
    t.integer "Tipo_dato",       limit: 2
    t.integer "Long_Despliegue", limit: 2
    t.varchar "Alias",           limit: 50
    t.integer "Posicion"
  end

  create_table "testadosxalerta", primary_key: "IdEstado", force: :cascade do |t|
    t.varchar "Descripcion",    limit: 60,   null: false
    t.integer "falso_positivo"
    t.varchar "mensajeDefault", limit: 1000
  end

  create_table "tfiltrosxgrupo", primary_key: ["IdGrupo", "IdFiltro"], force: :cascade do |t|
    t.integer "IdGrupo",    limit: 2,   null: false
    t.integer "IdFiltro",   limit: 2,   null: false
    t.varchar "Condicion",  limit: 255
    t.varchar "Operador",   limit: 60
    t.varchar "Valor1",     limit: 255
    t.varchar "Parentesis", limit: 20,  null: false
    t.integer "Negacion",               null: false
    t.varchar "valor2",     limit: 255
    t.integer "IdCampo",    limit: 2,   null: false
    t.varchar "IdToken",    limit: 5
    t.varchar "Conector",   limit: 10
    t.varchar "Query",      limit: 255, null: false
    t.varchar "TipoDato",   limit: 255
  end

  create_table "tformatos", primary_key: ["IdLay", "IdFormato"], force: :cascade do |t|
    t.integer "IdLay",       limit: 2,  null: false
    t.integer "IdFormato",   limit: 2,  null: false
    t.varchar "Descripcion", limit: 60, null: false
  end

  create_table "tgrupos", primary_key: "IdGrupo", force: :cascade do |t|
    t.integer "IdLay",          limit: 2,   null: false
    t.integer "IdFormato",      limit: 2,   null: false
    t.varchar "Nombre",         limit: 60,  null: false
    t.varchar "CadenaConexion", limit: 200
    t.varchar "Tabla",          limit: 60
    t.varchar "Campo",          limit: 60
    t.integer "Funcion",        limit: 2,   null: false
    t.integer "Activo",                     null: false
    t.integer "cancelacion",    limit: 2
  end

  create_table "tlay", primary_key: "IdLay", force: :cascade do |t|
    t.varchar "Descripcion", limit: 36
  end

  create_table "tperfiles", primary_key: ["IdGrupo", "IdPerfil"], force: :cascade do |t|
    t.integer "IdGrupo",  limit: 2,  null: false
    t.integer "IdPerfil", limit: 2,  null: false
    t.varchar "Nombre",   limit: 60
    t.varchar "FechaReg", limit: 10
    t.integer "Activo"
    t.integer "Batch"
  end

  create_table "trcestatus", primary_key: "Id", force: :cascade do |t|
    t.varchar  "Fecha",                  limit: 10
    t.varchar  "Hora",                   limit: 12
    t.bigint   "BloquesRecibidos"
    t.bigint   "TransaccionesRecibidas"
    t.varchar  "FechaHoraInicio",        limit: 24
    t.integer  "Desconexiones"
    t.varchar  "FechaHoraUltDesconex",   limit: 24
    t.integer  "Reconexiones"
    t.varchar  "FechaHoraUltReconex",    limit: 24
    t.integer  "Reiniciar",              limit: 2
    t.integer  "NoReinicios"
    t.varchar  "FechaHoraUltReinicio",   limit: 24
    t.varchar  "UltimoMensaje",          limit: 800
    t.datetime "created_at"
    t.datetime "update_at"
  end

  create_table "ttiporegalarma", primary_key: "IdTipoReg", force: :cascade do |t|
    t.varchar "Descripcion", limit: 50
  end

  create_table "tx_filter_details", force: :cascade do |t|
    t.integer "filter_id"
    t.integer "conditionnumber"
    t.varchar "connector",       limit: 45
    t.varchar "denied",          limit: 45
    t.varchar "operator",        limit: 45
    t.varchar "parenthesis",     limit: 45
    t.varchar "value",           limit: 500
    t.integer "fieldFormat_id"
    t.integer "position"
    t.integer "lenght"
    t.integer "key_id"
    t.string  "token_id"
  end

  create_table "tx_filters", primary_key: "Id", force: :cascade do |t|
    t.integer "Session_id"
    t.integer "Group_id"
    t.varchar "Description",     limit: 400
    t.integer "Lettercolor"
    t.integer "Order_number"
    t.integer "Key_id"
    t.char    "Inactive",        limit: 1
    t.string  "Backgroundcolor"
  end

  create_table "tx_sessions", primary_key: "Id", force: :cascade do |t|
    t.integer "Lay_id"
    t.integer "Format_id"
    t.varchar "Description",  limit: 500
    t.integer "Sessioncolor"
    t.integer "Interval"
    t.varchar "Inactive",     limit: 1
    t.varchar "Update",       limit: 1
    t.integer "Sessiontype",  limit: 2
  end

  create_table "txfilterxusr", primary_key: "Id", force: :cascade do |t|
    t.integer "Txfiltro_id", limit: 2
    t.integer "Prioridad"
    t.integer "IdUsuario"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                            default: "", null: false
    t.string   "encrypted_password",               default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                    default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",                  default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "password_changed_at"
    t.string   "unique_session_id",      limit: 1
    t.datetime "last_activity_at"
    t.datetime "expired_at"
    t.string   "user_name"
    t.string   "name"
    t.string   "last_name"
    t.date     "birthday"
    t.string   "boss_name"
    t.string   "phone"
    t.string   "ext"
    t.integer  "profile_id"
    t.string   "area_id"
    t.string   "employment"
    t.integer  "activo"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "habilitado"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["user_name"], name: "index_users_on_user_name", unique: true
  end

  create_table "views", force: :cascade do |t|
    t.string   "name"
    t.integer  "crear"
    t.integer  "editar"
    t.integer  "eliminar"
    t.integer  "leer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "white_lists", force: :cascade do |t|
    t.integer  "IdUsuario"
    t.string   "IdTran"
    t.string   "Fecha"
    t.string   "Hora"
    t.boolean  "Estado"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
